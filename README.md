# whitesur-icon-theme

MacOS Big Sur like icon theme for linux desktops

https://github.com/vinceliuice/WhiteSur-icon-theme

Do not compile in chroot environment

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/icons-and-themes/whitesur-icon-theme.git
```

